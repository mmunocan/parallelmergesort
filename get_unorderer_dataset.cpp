#include <iostream>

#include "libs/utilities.hpp"

using namespace std;

int main(int argc, char * argv[]){
	if(argc != 5){
		cout << "ERROR!! USE " << argv[0] << " <FILENAME> <CANT> <MIN> <MAX> donde:\n"
			 <<"<FILENAME> nombre del archivo de salida (es un binario)\n"
			 <<"<CANT> Cantidad de elementos a generar.\n"
			 <<"<MIN> y <MAX> Rango de valores a generar.\n";
		return -1;
	}
	
	char * filename = argv[1];
	int cant = atoi(argv[2]);
	int min = atoi(argv[3]);
	int max = atoi(argv[4]);
	
	float * datas = getRandomData(cant, min, max);
	//printArray(datas);
	writeArray(datas, filename);
	
	cout << "El archivo " << filename << " está escrito" << endl;
	
	return 0;
}