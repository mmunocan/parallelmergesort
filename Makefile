CPP=g++

OBJECTS=./libs/utilities.o ./libs/merge_sort.o
BINS=get_unorderer_dataset get_dataset_from_file get_merge_sort_2 get_merge_sort_4 get_time\
     get_parallel_merge_sort_2 get_parallel_merge_sort_4 get_time_recursive get_time_parallel\
	 get_time_thread
	 
CPPFLAGS=-Wall -O3 -DNDEBUG -fopenmp
DEST=.

%.o: %.cpp
	$(CPP) $(CPPFLAGS) -c $< -o $@

all: clean bin

bin: $(OBJECTS) $(BINS)
	
get_unorderer_dataset:
	$(CPP) $(CPPFLAGS) -o $(DEST)/get_unorderer_dataset get_unorderer_dataset.cpp $(OBJECTS)
	
get_dataset_from_file:
	$(CPP) $(CPPFLAGS) -o $(DEST)/get_dataset_from_file get_dataset_from_file.cpp $(OBJECTS)
	
get_merge_sort_2:
	$(CPP) $(CPPFLAGS) -o $(DEST)/get_merge_sort_2 get_merge_sort_2.cpp $(OBJECTS) 

get_merge_sort_4:
	$(CPP) $(CPPFLAGS) -o $(DEST)/get_merge_sort_4 get_merge_sort_4.cpp $(OBJECTS)

get_time:
	$(CPP) $(CPPFLAGS) -o $(DEST)/get_time get_time.cpp $(OBJECTS)
	
get_parallel_merge_sort_2:
	$(CPP) $(CPPFLAGS) -o $(DEST)/get_parallel_merge_sort_2 get_parallel_merge_sort_2.cpp $(OBJECTS)
	
get_parallel_merge_sort_4:
	$(CPP) $(CPPFLAGS) -o $(DEST)/get_parallel_merge_sort_4 get_parallel_merge_sort_4.cpp $(OBJECTS)

get_time_recursive:
	$(CPP) $(CPPFLAGS) -o $(DEST)/get_time_recursive get_time_recursive.cpp $(OBJECTS)

get_time_parallel:
	$(CPP) $(CPPFLAGS) -o $(DEST)/get_time_parallel get_time_parallel.cpp $(OBJECTS)
	
get_time_thread:
	$(CPP) $(CPPFLAGS) -o $(DEST)/get_time_thread get_time_thread.cpp $(OBJECTS)

clean:
	rm -f $(OBJECTS) $(BINS)
	cd $(DEST); rm -f $(BINS)
	