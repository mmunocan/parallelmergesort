#! /usr/bin/env bash

# Dont forget
# make

FILE="data/arr_"

./get_unorderer_dataset $FILE"10" 10000000 0 10000000
./get_unorderer_dataset $FILE"50" 50000000 0 50000000
./get_unorderer_dataset $FILE"100" 100000000 0 100000000
./get_unorderer_dataset $FILE"500" 500000000 0 500000000
./get_unorderer_dataset $FILE"1000" 1000000000 0 1000000000
./get_unorderer_dataset $FILE"5000" 5000000000 0 5000000000