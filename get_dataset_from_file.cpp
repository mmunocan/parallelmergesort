#include <iostream>

#include "libs/utilities.hpp"

using namespace std;

int main(int argc, char * argv[]){
	if(argc != 2){
		cout << "ERROR!! USE " << argv[0] << " <FILENAME> donde:\n"
			 <<"<FILENAME> nombre del archivo a leer (es un binario)\n";
		return -1;
	}
	char * filename = argv[1];
	
	float * datas;
	datas = readArray(filename);
	printArray(datas);
	
	delete [] datas;
	
	return 0;
}