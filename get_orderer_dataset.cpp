#include <iostream>

#include "libs/utilities.hpp"

using namespace std;

int main(int argc, char * argv[]){
	if(argc != 4){
		cout << "ERROR!! USE " << argv[0] << " <FILENAME> <CANT> <MIN> <MAX> donde:\n"
			 <<"<FILENAME> nombre del archivo de salida (es un binario)\n"
			 <<"<CANT> Cantidad de elementos a generar.\n"
			 <<"<MIN> Valor inicial de la secuencia.\n";
		return -1;
	}
	
	char * filename = argv[1];
	int cant = atoi(argv[2]);
	int min = atoi(argv[3]);
	
	float * datas;
	getOrdererData(datas, cant, min);
	//printArray(datas);
	writeArray(datas, filename);
	
	cout << "El archivo " << filename << " está escrito" << endl;
	
	return 0;
}