#include <iostream>
#include <chrono>

#include "libs/utilities.hpp"
#include "libs/merge_sort.hpp"

#define N 10

using namespace std;

int main(int argc, char * argv[]){
	if(argc != 3){
		cout << "ERROR!! USE " << argv[0] << " <FILENAME> <M> donde:\n"
			 <<"<FILENAME> nombre del archivo a leer (es un binario)\n"
			 <<"<M> tamaño porción para caso base\n";
		return -1;
	}
	char * filename = argv[1];
	int m = atoi(argv[2]);
	
	float * datas;
	auto start = chrono::high_resolution_clock::now();
	auto finish = chrono::high_resolution_clock::now();
	long t1, t2;
	
	t1 = 0;
	for(int i = 0; i < N; i++){
		datas = readArray(filename);
		start = chrono::high_resolution_clock::now();
		mergeSort2(datas, m);
		finish = chrono::high_resolution_clock::now();
		t1 += chrono::duration_cast<chrono::milliseconds> (finish - start).count();
		if(!isOrderer(datas)){
			cout << "El dataset no fue ordenado correctamente con mergeSort2." << endl;
		}
		delete [] datas;
	}
	t1 /= N;
	
	t2 = 0;
	for(int i = 0; i < N; i++){
		datas = readArray(filename);
		start = chrono::high_resolution_clock::now();
		mergeSort4(datas, m);
		finish = chrono::high_resolution_clock::now();
		t2 += chrono::duration_cast<chrono::milliseconds> (finish - start).count();
		if(!isOrderer(datas)){
			cout << "El dataset no fue ordenado correctamente con mergeSort4." << endl;
		}
		delete [] datas;
	}
	t2 /= N;
	
	printf("%d,%ld,%ld\n", m, t1, t2);
	
	return 0;
}