#include <iostream>
#include <chrono>

#include "libs/utilities.hpp"
#include "libs/merge_sort.hpp"

#define N 10

using namespace std;

int main(int argc, char * argv[]){
	if(argc != 5){
		cout << "ERROR!! USE " << argv[0] << " <FILENAME> <M> <M1> <N> donde:\n"
			 <<"<FILENAME> nombre del archivo a leer (es un binario)\n"
			 <<"<M> tamaño porción para caso base\n"
			 <<"<M1> tamaño porción para caso base paralelo\n"
			 <<"<N> cantidad de hebras\n";
		return -1;
	}
	char * filename = argv[1];
	int m = atoi(argv[2]);
	int m1 = atoi(argv[3]);
	int n = atoi(argv[4]);
	
	float * datas;
	auto start = chrono::high_resolution_clock::now();
	auto finish = chrono::high_resolution_clock::now();
	long t3, t4;
		
	t3 = 0;
	for(int i = 0; i < N; i++){
		datas = readArray(filename);
		start = chrono::high_resolution_clock::now();
		parallelMergeSort2(datas, m, m1, n);
		finish = chrono::high_resolution_clock::now();
		t3 += chrono::duration_cast<chrono::milliseconds> (finish - start).count();
		if(!isOrderer(datas)){
			cout << "El dataset no fue ordenado correctamente con parallelMergeSort2." << endl;
		}
		delete [] datas;
	}
	t3 /= N;
	
	t4 = 0;
	for(int i = 0; i < N; i++){
		datas = readArray(filename);
		start = chrono::high_resolution_clock::now();
		parallelMergeSort4(datas, m, m1, n);
		finish = chrono::high_resolution_clock::now();
		t4 += chrono::duration_cast<chrono::milliseconds> (finish - start).count();
		if(!isOrderer(datas)){
			cout << "El dataset no fue ordenado correctamente con parallelMergeSort4." << endl;
		}
		delete [] datas;
	}
	t4 /= N;
	
	printf("%d,%ld,%ld\n", n, t3, t4);
	
	return 0;
}