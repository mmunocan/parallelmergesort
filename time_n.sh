#! /usr/bin/env bash

# Dont forget
# make

FILE="data/arr_100"
M=65536
M1=262144

printf "n,mergeSort2,mergeSort4\n"
./get_time_thread $FILE $M $M1 1
./get_time_thread $FILE $M $M1 2
./get_time_thread $FILE $M $M1 3
./get_time_thread $FILE $M $M1 4
./get_time_thread $FILE $M $M1 5
./get_time_thread $FILE $M $M1 6
./get_time_thread $FILE $M $M1 7
./get_time_thread $FILE $M $M1 8
./get_time_thread $FILE $M $M1 9
./get_time_thread $FILE $M $M1 10
./get_time_thread $FILE $M $M1 11
./get_time_thread $FILE $M $M1 12
./get_time_thread $FILE $M $M1 13
./get_time_thread $FILE $M $M1 14
./get_time_thread $FILE $M $M1 15
./get_time_thread $FILE $M $M1 16
./get_time_thread $FILE $M $M1 17
./get_time_thread $FILE $M $M1 18
./get_time_thread $FILE $M $M1 19
./get_time_thread $FILE $M $M1 20


./get_time_thread $FILE $M $M1 40
./get_time_thread $FILE $M $M1 60
./get_time_thread $FILE $M $M1 80
./get_time_thread $FILE $M $M1 100