#include <cmath>
#include <vector>
#include <algorithm>
#include <omp.h>

using namespace std;

void mergeSort2(float * datas, int m);
void recursiveMergeSort2(float * datas, int i, int f, int m);
void mergeSort4(float * datas, int m);
void recursiveMergeSort4(float * datas, int i, int f, int m);

void parallelMergeSort2(float * datas, int m, int m1, int n);
void parallelRecursiveMergeSort2(float * datas, int i, int f, int m, int m1);
void parallelMergeSort4(float * datas, int m, int m1, int n);
void parallelRecursiveMergeSort4(float * datas, int i, int f, int m, int m1);

void merge2(float * datas, int i, int p, int f);
void merge4(float * datas, int i, int p1, int p2, int p3, int f);

void sequencialSort(float * datas, int i, int f);