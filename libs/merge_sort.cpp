#include "merge_sort.hpp"


void mergeSort2(float * datas, int m){
	recursiveMergeSort2(datas, 1, (int)datas[0]+1, m);
}

// trabaja con datas[i..f-1]
void recursiveMergeSort2(float * datas, int i, int f, int m){
	if((f-i) <= m){	// Cantidad caso base
		sequencialSort(datas, i, f);
		return;
	}
	
	int p = i + (f-i) / 2;
	recursiveMergeSort2(datas, i, p, m);
	recursiveMergeSort2(datas, p, f, m);
	
	// Merge
	merge2(datas, i, p, f);
	
}

void mergeSort4(float * datas, int m){
	recursiveMergeSort4(datas, 1, (int)datas[0]+1, m);
}

// trabaja con datas[i..f-1]
void recursiveMergeSort4(float * datas, int i, int f, int m){
	if((f-i) <= m){	// Cantidad caso base
		sequencialSort(datas, i, f);
		return;
	}
		
	int r = floor((f-i) / 4);	// porcion array para recursividad
	int p1 = i + r;
	int p2 = i + 2*r;
	int p3 = i + 3*r;
	
	recursiveMergeSort4(datas, i, p1, m);
	recursiveMergeSort4(datas, p1, p2, m);
	recursiveMergeSort4(datas, p2, p3, m);
	recursiveMergeSort4(datas, p3, f, m);
	
	// Merge
	merge4(datas, i, p1, p2, p3, f);
	
}

void parallelMergeSort2(float * datas, int m, int m1, int n){
	//omp_set_num_threads(20);
	#pragma omp parallel num_threads(n)
	{
		#pragma omp single
		{
			parallelRecursiveMergeSort2(datas, 1, (int)datas[0]+1, m, m1);
			
		}
	}
	
}

// trabaja con datas[i..f-1]
void parallelRecursiveMergeSort2(float * datas, int i, int f, int m, int m1){
	if((f-i) <= m1){	// Cantidad caso base para pasar al secuencial
		recursiveMergeSort2(datas, i, f, m);
		return;
	}
	
	int p = i + floor((f-i) / 2);
	
	#pragma omp task 
	{
		parallelRecursiveMergeSort2(datas, i, p, m, m1);
	}
	#pragma omp task
	{
		parallelRecursiveMergeSort2(datas, p, f, m, m1);
	}
	// Merge
	#pragma omp taskwait
	merge2(datas, i, p, f);
	
}

void parallelMergeSort4(float * datas, int m, int m1, int n){
	#pragma omp parallel num_threads(n)
	{
		#pragma omp single
		{
			parallelRecursiveMergeSort4(datas, 1, (int)datas[0]+1, m, m1);
		}
	}
}

void parallelRecursiveMergeSort4(float * datas, int i, int f, int m, int m1){
	if((f-i) <= m1){	// Cantidad caso base para pasar al secuencial
		recursiveMergeSort4(datas, i, f, m);
		return;
	}
		
	int r = floor((f-i) / 4);	// porcion array para recursividad
	int p1 = i + r;
	int p2 = i + 2*r;
	int p3 = i + 3*r;
	
	#pragma omp task 
	{
		parallelRecursiveMergeSort4(datas, i, p1, m, m1);
	}
	#pragma omp task 
	{
		parallelRecursiveMergeSort4(datas, p1, p2, m, m1);
	}
	#pragma omp task 
	{
		parallelRecursiveMergeSort4(datas, p2, p3, m, m1);
	}
	#pragma omp task 
	{
		parallelRecursiveMergeSort4(datas, p3, f, m, m1);
	}
	// Merge
	#pragma omp taskwait
	merge4(datas, i, p1, p2, p3, f);
}

void merge2(float * datas, int i, int p, int f){
	vector<float> helper;
	int ind1 = i; // [i..p-1]
	int ind2 = p; // [p..f-1]
	bool indMen;
	bool inserted;
	while((int)helper.size() < f-i){
		inserted = false;
		// Comprobar si datas[ind1] es menor
		indMen = ind1 < p;	 // si terminó el recorrido de porcion 1, no entraran mas
		if(ind2 < f) indMen = indMen && datas[ind1] <= datas[ind2];
		if(indMen){
			helper.push_back(datas[ind1]);
			ind1++;
			inserted = true;
		}
		
		if(!inserted){
			helper.push_back(datas[ind2]);
			ind2++;
			inserted = true;
		}
	}
	
	
	for(int j = i; j < f; j++){
		datas[j] = helper[j-i];
	}
	
}

void merge4(float * datas, int i, int p1, int p2, int p3, int f){
	vector<float> helper;
	int ind1 = i;  // [i..p1-1]
	int ind2 = p1; // [p1..p2-1]
	int ind3 = p2; // [p2..p3-1]
	int ind4 = p3; // [p3..p4-1]
	bool indMen;
	bool inserted;
	while((int)helper.size() < f-i){
		inserted = false;
		// Comprobar si datas[ind1] es menor
		indMen = ind1 < p1;	 // si terminó el recorrido de porcion 1, no entraran mas
		if(ind2 < p2) indMen = indMen && datas[ind1] <= datas[ind2];
		if(ind3 < p3) indMen = indMen && datas[ind1] <= datas[ind3];
		if(ind4 < f) indMen = indMen && datas[ind1] <= datas[ind4];
		if(indMen){
			helper.push_back(datas[ind1]);
			ind1++;
			inserted = true;
		}
		
		
		if(!inserted){
			// Comprobar si datas[ind2] es menor
			indMen = ind2 < p2;	 // si terminó el recorrido de porcion 2, no entraran mas
			if(ind1 < p1) indMen = indMen && datas[ind2] <= datas[ind1];
			if(ind3 < p3) indMen = indMen && datas[ind2] <= datas[ind3];
			if(ind4 < f) indMen = indMen && datas[ind2] <= datas[ind4];
			if(indMen){
				helper.push_back(datas[ind2]);
				ind2++;
				inserted = true;
			}
		}
		
		if(!inserted){
			// Comprobar si datas[ind3] es menor
			indMen = ind3 < p3;	 // si terminó el recorrido de porcion 3, no entraran mas
			if(ind1 < p1) indMen = indMen && datas[ind3] <= datas[ind1];
			if(ind2 < p2) indMen = indMen && datas[ind3] <= datas[ind2];
			if(ind4 < f) indMen = indMen && datas[ind3] <= datas[ind4];
			if(indMen){
				helper.push_back(datas[ind3]);
				ind3++;
				inserted = true;
			}
		}
		
		// Insertar datas[ind4] por default
		if(!inserted){
			helper.push_back(datas[ind4]);
			ind4++;
			inserted = true;
		}
	}
	
	for(int j = i; j < f; j++){
		datas[j] = helper[j-i];
	}
}

void sequencialSort(float * datas, int i, int f){
	sort(datas+i, datas+f);
}
