#include "utilities.hpp"

// datas[0] contiene cantidad de elementos
float * getRandomData(int cant, int min, int max){
	float * datas = new float[cant+1];
	datas[0] = (float)cant;
	srand(time(NULL));
	float random;
	for(int i = 1; i < cant+1; i++){
		random = ((float)rand()) / (float)RAND_MAX;
		datas[i] = (float)min + (random * (max-min));
	}
	return datas;
}

void printArray(const float * datas){
	cout << "Datos del array: " << endl;
	int cant = (int)datas[0];
	for(int i = 1; i < cant+1; i++){
		printf("%.6f ", datas[i]);
	}
	printf("\n");
}

void writeArray(const float * datas, const char * filename){
	ofstream f;
	f.open(filename);
	int cant = (int) datas[0];
	f.write((char *)(&cant), sizeof(int));	// El primer valor es la cantidad de elementos del archivo
	for(int i = 1; i < cant+1; i++){
		f.write((char *)(&datas[i]), sizeof(float));
	}
	f.close();
}

float * readArray(const char * filename){
	ifstream f;
	f.open(filename);
	int cant = 0;
	f.read((char *) (& cant), sizeof(int));
	float m = 0;
	float * datas = new float[cant+1];
	datas[0] = (float)cant;
	for(int i = 1; i < cant+1; i++){
		f.read((char *) (& m), sizeof(float));
		datas[i] = m;
	}
	f.close();
	return datas;
}

bool isOrderer(const float * datas){
	int cant = (int)datas[0];
	for(int i = 1; i < cant; i++){
		if(datas[i] > datas[i+1]){
			return false;
		}
	}
	return true;
}
