#include <iostream>
#include <cstdlib>
#include <ctime>
#include <fstream>

using namespace std;

float * getRandomData(int cant, int min, int max);
void printArray(const float * datas);
void writeArray(const float * datas, const char * filename);
float * readArray(const char * filename);
bool isOrderer(const float * datas);