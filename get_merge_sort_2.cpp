#include <iostream>
#include <chrono>

#include "libs/utilities.hpp"
#include "libs/merge_sort.hpp"

using namespace std;

int main(int argc, char * argv[]){
	if(argc != 4){
		cout << "ERROR!! USE " << argv[0] << " <FILENAME> <M> <O> donde:\n"
			 <<"<FILENAME> nombre del archivo a leer (es un binario)\n"
			 <<"<M> tamaño porción para caso base\n"
			 <<"<O>-> 0: no imprimir arrays | 1: imprimir arrays\n";
		return -1;
	}
	char * filename = argv[1];
	int m = atoi(argv[2]);
	int o = atoi(argv[3]);
	
	float * datas;
	datas = readArray(filename);
	if(o == 1){
		cout << "Array antes de ordenar: " << endl;
		printArray(datas);
	}
	auto start = chrono::high_resolution_clock::now();
	mergeSort2(datas, m);
	auto finish = chrono::high_resolution_clock::now();
	auto d = chrono::duration_cast<chrono::milliseconds> (finish - start).count();
	if(o == 1){
		cout << "Array despues de ordenar: " << endl;
		printArray(datas);
	}
	
	if(isOrderer(datas)){
		cout << "El array está ordenado" << endl;
	}else{
		cout << "El array NO está ordenado" << endl;
	}
	
	cout << "Demoró " << d << " milliseconds " << endl;
	
	delete [] datas;
	
	return 0;
}