#! /usr/bin/env bash

# Dont forget
# make

FILE="data/arr_"
M=65536
M1=262144
N=16

printf "cant,mergeSort2,mergeSort4,parallelMergeSort2,parallelMergeSort4\n"
./get_time $FILE"10" $M $M1 $N
./get_time $FILE"50" $M $M1 $N
./get_time $FILE"100" $M $M1 $N
./get_time $FILE"500" $M $M1 $N
./get_time $FILE"1000" $M $M1 $N
./get_time $FILE"5000" $M $M1 $N